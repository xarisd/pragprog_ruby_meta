animal = "test"

def animal.speak
  puts "roar"
end

cat = animal.clone

cat.speak

def animal.walk
  puts "walking"
end

cat = animal.clone
cat.walk