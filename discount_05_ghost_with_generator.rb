class Discounter
  
  def discount(*skus)
    expensive_discount_calculation(*skus)
  end
  
  private 
  
  def expensive_discount_calculation(*skus)
    puts "Expensive calculation for #{skus.inspect}"
    skus.inject {|m,n| m + n }
  end
end

def memoize(obj, method)
  # Get the ghost class
  ghost = class << obj; self; end
  
  # If I had used the obj.class then I would have OVEWRITTEN the method 
  # so there would have been no "super" method
  ghost.class_eval do
    memory ||= {} # create the memory if it does not exist

    define_method(method) do |*args|
      if memory.has_key?(args)
        memory[args]
      else
        memory[args] = super
      end  
    end
  end
end


d = Discounter.new
memoize(d, :discount)



puts d.discount(1,2,3)
puts d.discount(1,2,3)
puts d.discount(2,3,4)
puts d.discount(2,3,4)
