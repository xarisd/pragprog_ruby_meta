module Memoize

  def remember(method)
    
    # construct the "original" name
    # here we use an invalid name (that cannot be called directly i.e. "obj.original discount")
    #  so it cannot be called by accident (it can only be called through "send")
    original_name = "original #{method}"

    # make an alias. (this makes two separate versions of the method)
    alias_method original_name, method

    # this is accessible to the block bellow
    memory = {}
    
    # redefine the method
    define_method(method) do |*args|
      if memory.has_key?(args)
        memory[args]
      else
        memory[args] = send(original_name,*args) # call original
      end  
    end
  
  end
end


class Discounter
  extend Memoize
  
  def discount(*skus)
    expensive_discount_calculation(*skus)
  end
  
  remember :discount
  
  private 
  
  def expensive_discount_calculation(*skus)
    puts "Expensive calculation for #{skus.inspect}"
    skus.inject {|m,n| m + n }
  end
end


d = Discounter.new



puts d.discount(1,2,3)
puts d.discount(1,2,3)
puts d.discount(2,3,4)
puts d.discount(2,3,4)
