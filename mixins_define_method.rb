module AccessorMethods
  
  def my_attr_accessor(name)
    ivar_name = "@#{name}"
    
    define_method(name) do 
      instance_variable_get(ivar_name)
    end

    define_method("#{name}=") do | val |
      STDERR.puts "#{ivar_name} instance variable got new val : #{val}"
      instance_variable_set(ivar_name, val)
    end
  end
  
end


module MyValidations
  
  def self.included(klass)
    klass.extend ClassExtensions
  end
  
  
  def isValid?
    result = true
    self.class.validations.each do | name, block |
      b = block.call(instance_variable_get("@#{name}"))
      @validation_errors ||= {}
      @validation_errors[name] = "#{name} is not valid"  unless b
      result &=  b
    end
    result
  end
  
  def validation_errors
    isValid?
    @validation_errors
  end
  
  def validate(name)
    block = self.class.validations[name]
    return block.call(instance_variable_get("@#{name}")) if block
    true
  end
  
  def validation_error_for(name)
    isValid? #
    @validation_errors[name]
  end
  
  module ClassExtensions
    def validates(name, &block)
      @validations ||= {}
      @validations[name]= block
    end
    def validations
      @validations
    end
  end
  
end

class Test
  include MyValidations
  extend AccessorMethods
  
  my_attr_accessor :title
  validates(:title) { |t| t.size > 3 }
end

t = Test.new

t.title = "Title 1"
puts t.title

puts "title is valid? #{t.validate(:title)}"  
puts t.isValid?
puts t.validation_errors.inspect unless t.isValid?

t.title = "T2"
puts t.title

puts "title is valid? #{t.validate(:title)}"  
puts t.isValid?
puts t.validation_errors.inspect unless t.isValid?
