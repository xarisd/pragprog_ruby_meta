# encoding: UTF-8

class Module
  
  original_c_m = instance_method(:const_missing)
  
  define_method (:const_missing) do |name|
    if name.to_s =~ /^U([0-9a-fA-F]{4})$/
      [$1.to_i(16)].pack("U*")
    else
      original_c_m.bind(self).call(name)
    end
  end
  
end


class Dave
  def self.const_missing(name)
    puts "Missing #{name}"
  end
end

puts U0123
puts U0041
puts Dave::Fred
puts Fred
