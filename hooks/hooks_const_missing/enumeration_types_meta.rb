# encoding: UTF-8

module Enum
  
  def self.new
    Class.new do 
      
      def initialize(name)
        @name = name
      end
      def to_s
        "#{self.class.name}->#{@name}"
      end
      
      def self.const_missing(name)
        const_set(name, new(name))
      end
    end
  end
  
end

Color = Enum.new
ThreatLevel = Enum.new

puts Color::Red
puts Color::Orange

puts Color::Red == Color::Orange
puts Color::Orange == Color::Orange

puts ThreatLevel::Orange != Color::Orange
puts ThreatLevel::Orange == ThreatLevel::Orange
