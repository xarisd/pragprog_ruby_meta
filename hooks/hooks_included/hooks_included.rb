# encoding: UTF-8

module Persistable
  module ClassMethods
    def find(params)
      puts "find called"
    end
  end
  
  def self.included(klass)
    klass.extend ClassMethods
  end
end

class Person
  include Persistable
end

Person.find(1)
