# encoding: UTF-8

# in Ruby 1.8 you cannot pass a &block inside a call to a second block

module TraceCalls 

  def self.included(klass)
    
    klass.const_set(:METHOD_HASH, {})
    
    suppress_tracing do
      klass.instance_methods(false).each do |name|
        wrap_method(klass, name.to_sym)
      end
    end
    
    def klass.method_added(name)
      return if @_adding_a_method
      @_adding_a_method = true
      TraceCalls.wrap_method(self, name)
      @_adding_a_method = false
    end
  end

  def self.suppress_tracing
    old = Thread.current[:"suppress tracing"]
    Thread.current[:"suppress tracing"] = true
    yield
  ensure
    Thread.current[:"suppress tracing"] = old
  end
  
  def self.ok_to_trace?
    !Thread.current[:"suppress tracing"]
  end
  
  def self.wrap_method(klass, name)

    method_hash = klass.const_get(:METHOD_HASH)
    method_object = klass.instance_method(name)
    method_hash[name] = method_object
    
    body = %{ 
      def #{name}(*args, &block)
        if TraceCalls.ok_to_trace?
          TraceCalls.suppress_tracing do
            puts "==> Calling '#{name}' with args '\#{args.inspect}'"
          end
        end
        method_object = METHOD_HASH[:"#{name}"]
        bound_method = method_object.bind(self)
        result = bound_method.call(*args, &block)
        if TraceCalls.ok_to_trace?
          TraceCalls.suppress_tracing do
            puts "<== result = \#{result}"
          end
        end
        result
      end
    }
    
    # puts body
    
    klass.class_eval body
    # puts "Added method #{name} with tracing"
  end
  
end

class Array
  include TraceCalls
end


class String 
  include TraceCalls
end

"cat" + "dog"

# class Example
#   include TraceCalls
#   
#   def some_method(arg1, arg2)
#     arg1 + arg2
#   end
#   
#   def method2(arg)
#     yield * arg
#   end
#   
#   def name=(name)
#     @name = name
#   end
#   
#   def <<(thing)
#     puts "pushing #{thing}"
#   end
# end
# 
# 
# ex = Example.new
# ex.some_method(4,5)
# ex.some_method(3,5)
# ex.method2(99) { 2 }
# ex.name = "fred"
# ex << 1