# encoding: UTF-8

module TraceCalls 

  def self.included(klass)
    
    suppress_tracing do
      klass.instance_methods(false).each do |name|
        wrap_method(klass, name.to_sym)
      end
    end
    
    def klass.method_added(name)
      return if @_adding_a_method
      @_adding_a_method = true
      TraceCalls.wrap_method(self, name)
      @_adding_a_method = false
    end
  end
  
  def self.wrap_method(klass, name)
    klass.class_eval do 
      name = name.to_s
      original_method = instance_method(name)
    
      define_method(name) do |*args, &block|
        if TraceCalls.ok_to_trace?
          TraceCalls.suppress_tracing do
            puts "==> Calling '#{name}' with args '#{args.inspect}'"
          end
        end
        bound_method = original_method.bind(self)
        result = bound_method.call *args, &block
        if TraceCalls.ok_to_trace?
          TraceCalls.suppress_tracing do
            puts "<== result = #{result}"
          end
        end
        result
      end
    end
    puts "Added tracing to method #{klass}.#{name}"
  end

  def self.suppress_tracing
    old = Thread.current[:"suppress tracing"]
    Thread.current[:"suppress tracing"] = true
    yield
  ensure
    Thread.current[:"suppress tracing"] = old
  end
  
  def self.ok_to_trace?
    !Thread.current[:"suppress tracing"]
  end

end



class Array
  include TraceCalls
end


class String 
  include TraceCalls
end

"cat" + "dog"

class Time 
  include TraceCalls
end

Time.now + 3600

class Example
  include TraceCalls
  
  def some_method(arg1, arg2)
    arg1 + arg2
  end
  
  def method2(arg)
    yield * arg
  end
end


ex = Example.new
ex.some_method(4,5)
ex.some_method(3,5)
ex.method2(99) { 2 }