# encoding: UTF-8

class Dave
  
  def self.method_added(name)
    puts "Added method #{name}"
  end
  
  def fred
  end
  
  def wilma
  end
  
end
