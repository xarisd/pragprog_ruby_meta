# encoding: UTF-8

class ShippingMediaMail < ShippingOption
  def self.can_ship?(weight, international)
    !international
  end
end