# encoding: UTF-8

class ShippingPriorityFlatRate < ShippingOption
  def self.can_ship?(weight, international)
    !international && weight < 4*16
  end
end