# encoding: UTF-8

class ShippingOption
  @children = [] # class variable
  
  def self.inherited(klass)
    @children << klass
  end
  
  def self.for(weight, international)
    @children.select do |child| 
      child.can_ship?(weight, international)
    end
  end
end

# Auto require the subclasses
Dir.glob("shipping_*.rb").each do |name|
  puts "requiring #{name}"
  require_relative name
end