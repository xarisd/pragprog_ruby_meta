class Parent
  def self.inherited(klass)
    puts "#{self} was inherited by #{klass}"
  end
end

class Child < Parent
  puts "Inside class #{self}"
end

class OtherChild < Parent
  puts "Inside class #{self}"
end
