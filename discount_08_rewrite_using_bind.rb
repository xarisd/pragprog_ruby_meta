module Memoize

  def remember(method)
    
    # construct a block object from the method
    # this is a "frozen" method not a pointer (like a copy)
    original_method = instance_method(method)

    # this is accessible to the block bellow
    memory = {}
    
    # redefine the method
    define_method(method) do |*args|
      if memory.has_key?(args)
        memory[args]
      else
        # bind the block to self 
        bound_method = original_method.bind(self)
        # and then call it with the args
        memory[args] = bound_method.call(*args)
      end  
    end
  
  end
end


class Discounter
  extend Memoize
  
  def discount(*skus)
    expensive_discount_calculation(*skus)
  end
  
  remember :discount
  
  private 
  
  def expensive_discount_calculation(*skus)
    puts "Expensive calculation for #{skus.inspect}"
    skus.inject {|m,n| m + n }
  end
end


d = Discounter.new



puts d.discount(1,2,3)
puts d.discount(1,2,3)
puts d.discount(2,3,4)
puts d.discount(2,3,4)
